<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Model Language Lines
    |--------------------------------------------------------------------------
    |
    */

    // SISTEMA
    'node' => 'Node|Nodes',
    'node-extra' => 'Node Extra|Node Extras',
    'node-request' => 'Node Request|Node Requests',
    'field' => 'Field|Fields',
    'field-conditional' => 'Field Conditional|Field Conditionals',
    'field-extra' => 'Field Extra|Field Extras',
    'permission' => 'Permission|Permissions',
    'role' => 'Role|Roles',
    'permission-role' => 'Permission/Role|Permissions/Roles',
    'role-user' => 'Role/User|Role/Users',
    'temp-file' => 'Temporal File|Temporal Files',
    'image-folder' => 'Image Folder|Images Folder',
    'image-size' => 'Image Size|Image Sizes',

    // GLOBAL
    'site' => 'Site|Sites',
    'user' => 'User|Users',
    'page' => 'Page|Pages',
    'menu' => 'Menu|Menus',
    'section' => 'Section|Sections',
    'variable' => 'Variable|Variables',
    'email' => 'Email|Emails',
    'activity' => 'Activity|Activities',
    'notification' => 'Notification|Notifications',

    // MENU
    'my_dashboard' => 'My Dashboard',
    'my_account' => 'My Account',
    'profile' => 'Profile',
    'logout' => 'Logout',

];
