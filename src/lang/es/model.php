<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Model Language Lines
    |--------------------------------------------------------------------------
    |
    */

    // SISTEMA
    'node' => 'Nodo|Nodos',
    'node-extra' => 'Extra de Nodo|Extras de Nodo',
    'node-request' => 'Request de Nodo|Requests de Nodo',
    'field' => 'Campo|Campos',
    'field-conditional' => 'Condicional de Campo|Condicionales de Campo',
    'field-extra' => 'Extra de Campo|Extras de Campo',
    'permission' => 'Permiso|Permisos',
    'role' => 'Rol|Roles',
    'permission-role' => 'Permiso/Rol|Permisos/Roles',
    'role-user' => 'Rol/Usuario|Roles/Usuarios',
    'temp-file' => 'Archivo Temporal|Archivos Temporales',
    'image-folder' => 'Folder de Imagen|Folders de Imágenes',
    'image-size' => 'Tamaño de Imagen|Tamaño de Imágenes',

    // GLOBAL
    'site' => 'Sitio|Sitios',
    'user' => 'Usuario|Usuarios',
    'page' => 'Página|Páginas',
    'menu' => 'Menú|Menús',
    'section' => 'Sección|Secciones',
    'variable' => 'Variable|Variables',
    'email' => 'Email|Emails',
    'activity' => 'Actividad|Actividades',
    'notification' => 'Notificación|Notificaciones',

    // MENU
    'my_dashboard' => 'Mi Panel',
    'my_account' => 'Mi Cuenta',
    'profile' => 'Perfil',
    'logout' => 'Cerrar Sesión',

];
