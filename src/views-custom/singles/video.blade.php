<div class="col-sm-4 col-xs-6 grid-item animated">
  <a class="lightbox" href="{{ $full }}">
	<div class="gallery_view">
	  <img class="img-responsive" src="{{ $thumb }}" />
	  <div class="mask">
	    <h3 class="info">{{ $name }}</h3>
	  </div>
	</div>
  </a>
</div>