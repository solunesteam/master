<?php

return array(

	"form-contact_success" => "Su mensaje de contacto fue recibido y será atendido a la brevedad posible.",
	"form-contact_fail" => "No se pudo enviar su mensaje por los siguientes errores.",
	"registry-a_success" => "Su formulario fue correctamente registrado. En breve le enviaremos su nombre de usuario y contraseña al correo de referencia. Este proceso puede tardar un par de horas.",
	"registry-a_fail" => "Usted no ha completado correctamente alguno de los campos obligatorios. Por favor revise  y vuelva a intentar.",
	"registry-b_success" => "Su formulario fue correctamente registrado. En breve le enviaremos su nombre de usuario y contraseña al correo de referencia. Este proceso puede tardar un par de horas.",
	"registry-b_fail" => "Usted no ha completado correctamente alguno de los campos obligatorios. Por favor revise  y vuelva a intentar.",
	"postulation-a_success" => "Su formulario fue correctamente enviado. Para conocer detalles del proceso de evaluación consulte las bases del concurso y las fechas en la página de inicio.",
	"postulation-a_fail" => "Usted no ha completado correctamente alguno de los campos obligatorios. Por favor revise  y vuelva a intentar.",
	"postulation-b_success" => "Su formulario fue correctamente enviado. Para conocer detalles del proceso de evaluación consulte las bases del concurso y las fechas en la página de inicio.",
	"postulation-b_fail" => "Usted no ha completado correctamente alguno de los campos obligatorios. Por favor revise  y vuelva a intentar.",

);
