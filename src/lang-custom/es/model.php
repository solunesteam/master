<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Model Language Lines
    |--------------------------------------------------------------------------
    |
    */

    // SISTEMA
    'user' => 'Usuario|Usuarios',

    // SECCIONES
    'social-network' => 'Red Social|Redes Sociales',
    'title' => 'Título|Títulos',
    'content' => 'Contenido|Contenidos',
    'form-ver' => 'Formulario|Formularios',
    'form-field' => 'Campo|Campos',
    'service' => 'Servicio|Servicios',
    'product' => 'Producto|Productos',
    'contact' => 'Contacto|Contacto',
    'photo' => 'Foto|Fotos',
    'video' => 'Video|Videos',
    'file' => 'Archivo|Archivos',
    'audio' => 'Audio|Audios',
    'article' => 'Artículo|Artículos',
    'publication' => 'Publicación|Publicaciones',
    'job' => 'Trabajo|Trabajos',
    'banner' => 'Banner|Banners',
    'project' => 'Proyecto|Proyectos',
    'sector' => 'Sector|Sectores',
    'member' => 'Miembro|Miembros',
    'location' => 'Ubicación|Ubicaciones',
    'agenda' => 'Agenda|Agendas',
    'sponsor' => 'Patrocinador|Patrocinadores',
    'deadline' => 'Fecha Límite|Fechas Límite',

    // FORMULARIOS
    'form-contact' => 'Formulario de Contacto|Formularios de Contacto',
    'registry-a' => 'Registro A|Registro A',
    'registry-b' => 'Registro B|Registro B',
    'postulation-a' => 'Postulación A|Postulaciones A',
    'postulation-b' => 'Postulación B|Postulaciones B',

];
