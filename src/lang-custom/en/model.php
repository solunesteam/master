<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Model Language Lines
    |--------------------------------------------------------------------------
    |
    */

    // SISTEMA
    'user' => 'User|Users',

    // SECTIONS
    'social-network' => 'Red Social|Redes Sociales',
    'title' => 'Title|Titles',
    'content' => 'Content|Content',
    'form-ver' => 'Form|Forms',
    'form-field' => 'Field|Fields',
    'service' => 'Service|Services',
    'product' => 'Product|Products',
    'contact' => 'Contact|Contact Forms',
    'photo' => 'Photo|Photos',
    'video' => 'Video|Videos',
    'file' => 'File|Files',
    'audio' => 'Audio|Audios',
    'article' => 'Article|Articles',
    'publication' => 'Publication|Publications',
    'job' => 'Job|Jobs',
    'banner' => 'Banner|Banners',
    'project' => 'Project|Projects',
    'sector' => 'Sector|Sectors',
    'member' => 'Member|Members',
    'location' => 'Location|Locations',
    'agenda' => 'Agenda|Agenda',
    'sponsor' => 'Sponsor|Sponsors',
    'deadline' => 'Deadline|Deadlines',

    // FORMS
    'form-contact' => 'Contact Form|Contact Forms',
    'registry-a' => 'Registro A|Registro A',
    'registry-b' => 'Registro B|Registro B',
    'postulation-a' => 'Postulación A|Postulaciones A',
    'postulation-b' => 'Postulación B|Postulaciones B',

];
